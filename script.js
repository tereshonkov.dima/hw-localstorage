const btnTheme = document.getElementById("theme-button");
const styleTheme = document.getElementById("style-theme");


btnTheme.addEventListener("click", function () {
    if (styleTheme.href.includes("/style-light.css")) {
        styleTheme.href = "/css/style-dark.css"; 
        localStorage.setItem('theme', '/css/style-dark.css');
    } else {
        styleTheme.href = "/css/style-light.css"; 
        localStorage.setItem('theme', '/css/style-light.css');
    };
});

const storedTheme = localStorage.getItem('theme');

if (storedTheme) {
  styleTheme.href = storedTheme;
} else {
  styleTheme.href = "/css/style-light.css";
};
